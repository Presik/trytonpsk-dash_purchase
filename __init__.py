# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import purchase
from . import configuration
from . import user
from . import dash


def register():
    Pool.register(
        purchase.Purchase,
        purchase.ForwardPurchaseMail,
        purchase.AppPurchase,
        purchase.AppPurchaseApproval,
        purchase.AppPurchaseRequisition,
        configuration.Configuration,
        configuration.UserAuthorizedPurchase,
        user.User,
        dash.DashApp,
        module='dash_purchase', type_='model')
    Pool.register(
        purchase.ForwardPurchaseMail,
        module='dash_purchase', type_='wizard')
