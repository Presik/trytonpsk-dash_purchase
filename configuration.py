# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pyson import Eval

STATES = {
    'invisible': Eval('require_authorized'),
}


class Configuration(metaclass=PoolMeta):
    __name__ = 'purchase.configuration'
    require_authorized = fields.Boolean('Purhase Required Authorization')
    authorized_users = fields.One2Many(
        'purchase.configuration.user_authorized',
        'configuration', 'Authorized Users')
    template_email_confirm = fields.Many2One('email.template', 'Template Email of Confirmation')

    @staticmethod
    def default_require_authorized():
        return False


class UserAuthorizedPurchase(ModelSQL, ModelView):
    'User Authorized Purchases'
    __name__ = 'purchase.configuration.user_authorized'
    configuration = fields.Many2One('purchase.configuration', 'Configuration',
        ondelete='CASCADE')
    active = fields.Boolean('Active')
    user = fields.Many2One('res.user', 'User', ondelete='RESTRICT',
        required=True)
    limit_amount_confirm = fields.Numeric("Limit Amount")

    @classmethod
    def __setup__(cls):
        super(UserAuthorizedPurchase, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
           ('user_uniq', Unique(table, table.user),
               'User already exists!'),
        ]

    @staticmethod
    def default_active():
        return True

    def get_rec_name(self, name):
        return self.user.name
